package com.hzaccp.openfiretest.biz;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;

import android.app.Activity;

/**
 * 客户端会话类<b>(单例模式)</b><br>
 * 类似web服务器上的session<br>
 * 保存登录用户的openfire连接、当前与谁在会话及客户端当前是哪个活动界面<br>
 * 
 * */
public class SE {
	private static SE se;
	private XMPPConnection con;//链接
	private String cu;//当前聊天用户

	private Activity userActivity;//用户列表界面
	private Activity chatActivity;//聊天会话界面

	private SE() {

	}

	public static SE getInstance() {
		if (se == null) {
			se = new SE();
		}
		return se;
	}

	/**
	 * 得到默认连接
	 * */
	public XMPPConnection getCon() {
		if (this.con == null) {
			return getCon("192.168.1.99", 5222);
		}
		return con;
	}

	/**
	 * 得到服务器连接
	 * */
	public XMPPConnection getCon(String ip, int port) {
		try {
			if (this.con == null) {
				ConnectionConfiguration connConfig = new ConnectionConfiguration(ip, port);
				con = new XMPPConnection(connConfig);
				con.connect();
			}
		} catch (XMPPException err) {
			err.printStackTrace();
		}
		return con;
	}

	/**
	 * 退出服务器
	 * */
	public void logOut() {
		con.disconnect();
	}

	public String getCu() {
		return cu;
	}

	public void setCu(String cu) {
		this.cu = cu;
	}

	public void setUserActivity(Activity userActivity) {
		this.userActivity = userActivity;
	}

	public Activity getUserActivity() {
		return userActivity;
	}

	public Activity getChatActivity() {
		return chatActivity;
	}

	public void setChatActivity(Activity chatActivity) {
		this.chatActivity = chatActivity;
	}
}
