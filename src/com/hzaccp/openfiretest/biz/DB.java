package com.hzaccp.openfiretest.biz;

import java.util.HashMap;

/**
 * 临时存储会话(单例模式)<br>
 * 实际应用中，应将mes改为数据库操作
 * */
public class DB {
	private static DB db;
	private HashMap<String, String> mes;

	private DB() {

	}

	public static DB getInstance() {
		if (db == null) {
			db = new DB();
		}
		return db;
	}

	public HashMap<String, String> getMes() {
		if (mes == null) {
			mes = new HashMap<String, String>();
		}
		return mes;
	}

	public void appMes(String form, String mess) {
		HashMap<String, String> map = getMes();
		if (map.containsKey(form)) {
			String mt = map.get(form) + "\r\n" + mess;
			map.remove(form);
			map.put(form, mt);
		} else {
			map.put(form, mess);
		}
	}

	public String getMess(String form) {
		HashMap<String, String> map = getMes();
		return map.get(form);
	}

	public void clearMes(String form) {
		getMes().remove(form);
	}
}
